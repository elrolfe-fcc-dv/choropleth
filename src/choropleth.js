let dataCounties;
let dataEducation;

const displayData = () => {
  const svg = d3.select("svg");
  const width = +svg.attr("width");
  const height = +svg.attr("height");

  const min = Math.ceil(d3.min(Object.keys(dataEducation), d => dataEducation[d].bachelorsOrHigher) / 10);
  const max = Math.floor(d3.max(Object.keys(dataEducation), d => dataEducation[d].bachelorsOrHigher) / 10);
  const color = d3.scaleThreshold()
    .domain(d3.range(min, max))
    .range(d3.schemeGnBu[max - min + 1]);

  const legendScale = d3.scaleLinear()
    .domain([min, max])
    .rangeRound([600, 860]);

  const legendAxis = d3.axisBottom(legendScale);

  const legend = svg.append("g")
    .attr("id", "legend")
    .attr("transform", "translate(0, 20)");

  legend.selectAll("rect")
    .data(color.range().map(d => {
      const r = color.invertExtent(d);
      if (typeof r[0] === "undefined") r[0] = legendScale.domain()[0];
      if (typeof r[1] === "undefined") r[1] = legendScale.domain()[1];
      return r;
    }))
    .enter()
    .append("rect")
      .attr("x", d => legendScale(d[0]))
      .attr("width", d => legendScale(d[1]) - legendScale(d[0]) + 1)
      .attr("height", 16)
      .attr("fill", d => color(d[0]));

  legend.call(
    legendAxis
      .tickSize(16)
      .tickFormat((d) => (d * 10) + "%")
      .tickValues(color.domain())
  );

  legend.selectAll("line")
    .attr("fill", "#ffffff");

  legend.selectAll("text")
    .attr("fill", "#ffffff");

  const path = d3.geoPath();
  svg.append("g")
    .attr("class", "counties")
    .selectAll("path")
      .data(topojson.feature(dataCounties, dataCounties.objects.counties).features)
      .enter()
      .append("path")
        .attr("class", "county")
        .attr("data-fips", d => d.id)
        .attr("data-education", d => dataEducation[d.id].bachelorsOrHigher)
        .attr("fill", d => color(dataEducation[d.id].bachelorsOrHigher / 10))
        .attr("d", path)
        .on("mouseover", d => {
          d3.select("#county").text(dataEducation[d.id].area_name);
          d3.select("#state").text(dataEducation[d.id].state);
          d3.select("#degree").text(dataEducation[d.id].bachelorsOrHigher);
          d3.select("#tooltip")
            .attr("data-education", dataEducation[d.id].bachelorsOrHigher)
            .style("left", (d3.event.pageX + 15) + "px")
            .style("top", (d3.event.pageY - 15) + "px")
            .style("opacity", 1);
        })
        .on("mouseout", () => {
          d3.select("#tooltip")
            .style("opacity", 0);
        });

  svg.append("path")
    .datum(topojson.mesh(dataCounties, dataCounties.objects.states, function(a, b) { return a !== b; }))
    .attr("class", "states")
    .attr("d", path);

  d3.select("#title").text("United States Education");
  d3.select("#description").text("Percentage of adults age 25 and older with a bachelor's degree or higher (2010-2014)")
};

const load = (url) => new Promise(resolve => {
  const req = new XMLHttpRequest();
  req.open("GET", url, true);
  req.onload = () => {
    resolve(JSON.parse(req.responseText));
  }
  req.send();
});

document.addEventListener("DOMContentLoaded", () => {
  d3.json("https://raw.githubusercontent.com/no-stack-dub-sack/testable-projects-fcc/master/src/data/choropleth_map/counties.json")
  .then((data) => {
    dataCounties = data;
    return d3.json("https://raw.githubusercontent.com/no-stack-dub-sack/testable-projects-fcc/master/src/data/choropleth_map/for_user_education.json")
  })
  .then((data) => {
    dataEducation = {};

    data.forEach(item => {
      dataEducation[item.fips] = item;
    });
    displayData();
  })
  .catch(err => {
    d3.select("#title").text("Error Loading Data");
    d3.select("#subtitle").text(err.message || err || "Unknown Error");
  });
});
